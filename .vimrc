syntax on
map <f7> :tabp <cr>
map <f8> :tabn <cr>
map <f9> :tabe
map <f3> :split <cr>
map <f4> :vsplit <cr>
set ts=2
set sw=2
set number
